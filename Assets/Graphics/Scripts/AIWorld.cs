﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWorld : MonoBehaviour {

    public World otherWorld;
    //public Rigidbody2D targetingReticle;
    Rigidbody2D otherWorldRb;

    float buyTimer = 0f;
    
    World world;
    Rigidbody2D worldRb;

    float LaunchTimer = 0f;
    float aimTimer = 0f;

    float desiredAngle = 0f;
    float desiredThrustDuration = 0f;
    float rotationDirection = 0f;

    float distance, aimAngle;

    bool hasThruster = false;
    bool hasRotation = false;
    bool hasLaunchPad = false;

	// Use this for initialization
	void Start () {
        world = GetComponent<World>();
        worldRb = world.GetComponent<Rigidbody2D>();
        otherWorldRb = otherWorld.GetComponent<Rigidbody2D>();
	}

    public void reset()
    {
        hasThruster = false;
        hasRotation = false;
        hasLaunchPad = false;
        buyTimer = 0f;
    }
	
	// Update is called once per frame
	void Update () {



        // rotation
        aimTimer += Time.deltaTime;
        if (aimTimer > 0.5f)
        {
            aimTimer = 0f;
            Vector2 aimTarget = otherWorldRb.position + (otherWorldRb.velocity * 2f);
            //targetingReticle.position = aimTarget;
            aimAngle = Mathf.Atan2(aimTarget.y - transform.position.y, aimTarget.x - transform.position.x) * Mathf.Rad2Deg;
            //targetingReticle.rotation = aimAngle;
            aimAngle += Random.Range(-30f, 30f);
            desiredAngle = aimAngle;
        }
         

        if (desiredAngle != worldRb.rotation && hasRotation)
        {
            rotationDirection = Mathf.Clamp((desiredAngle - worldRb.rotation) / 10, -1f, 1f);
            world.rotateWorld(rotationDirection);
        }

        // thruster
        if (world.transform.position.x > 4 && hasThruster)
            world.activateThrust();


        // missile
        LaunchTimer += Time.deltaTime;
        if (world.isRocketLoaded() && LaunchTimer > Random.Range(3f, 6f))
        {
            world.launchRocket();
            LaunchTimer = 0f;
        }




        // purchases
        buyTimer += Time.deltaTime;

        if (buyTimer > 18 && !hasLaunchPad)
        {
            Debug.Log("AIWorld :: buying launchpad");
            world.buyLaunchPad();
            hasLaunchPad = true;
        }
        else if (buyTimer > 9 && !hasThruster)
        {
            world.buyThruster();
            hasThruster = true;
        }

        else if (buyTimer > 5 && !hasRotation)
        {
            world.buyRotation();
            hasRotation = true;
        }
    }
}
