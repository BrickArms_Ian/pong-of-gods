﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    Text p1Score, p1Rotator, p1Thruster, p1LaunchPad, p1Influence;
    Text p2Score, p2Rotator, p2Thruster, p2LaunchPad, p2Influence;

    float rocketReload = 2f;
    float reloadTimer = 2f;
    bool isLoaded = true;

    string missileReadyStr = "READY TO LAUNCH MISSILE  (press 'f')";
    string rotatorDefaultStr;
    string thrusterDefaultStr;
    string missileDefaultStr;

    void Start()
    {

        // player 1
        Transform p1Container = transform.FindChild("Player1");
        p1Score = p1Container.FindChild("Score").GetComponent<Text>();
        p1Rotator = p1Container.FindChild("Rotator").GetComponent<Text>();
        p1Thruster = p1Container.FindChild("Thruster").GetComponent<Text>();
        p1LaunchPad = p1Container.FindChild("LaunchPad").GetComponent<Text>();
        p1Influence = p1Container.FindChild("Influence").GetComponent<Text>();

        rotatorDefaultStr = p1Rotator.text;
        thrusterDefaultStr = p1Thruster.text;
        missileDefaultStr = p1LaunchPad.text;

        // player 2
        
        Transform p2Container = transform.FindChild("Player2");
        p2Score = p2Container.FindChild("Score").GetComponent<Text>();
        /* // TODO: some p2 UI components init commented out!
        p2Rotator = p2Container.FindChild("Rotator").GetComponent<GUIText>();
        p2Thruster = p2Container.FindChild("Thruster").GetComponent<GUIText>();
        p2LaunchPad = p2Container.FindChild("LaunchPad").GetComponent<GUIText>();
        */

    }

    void Update()
    {
        if (!isLoaded)
        {
            if (reloadTimer < 0)
            {
                isLoaded = true;
                p1LaunchPad.text = "READY TO LAUNCH MISSILE  (press 'f')";
            }
            else
            {
                p1LaunchPad.text = "Missile Rebuilding... " + reloadTimer.ToString("0.00");
                reloadTimer -= Time.deltaTime;
            }
        }
    }

    public void updateScore(int playerNumber, int amount) // TODO: This is going from World to GameManager to UIManager, maybe don't do that?
    {
        if (playerNumber == 1)
        {
            p1Score.text = "" + amount;
        }
        else
            p2Score.text = "" + amount;
    }

    public void updateInfluence(int playerNumber, float amount)
    {
        if (playerNumber == 1)
        {
            p1Influence.text = "Influence: " + amount.ToString("0") + "i";
        }
        //else
            //p2Influence.text = "" + amount;
    }

    public void rotatorPurchased(int playerNumber)
    {
        if (playerNumber == 1)
            p1Rotator.text = "Use 'q' and 'e' to rotate your world";
    }

    public void launchPadPurchased(int playerNumber)
    {
        if (playerNumber == 1)  
            p1LaunchPad.text = "READY TO LAUNCH MISSILE  (press 'f')";
    }

    public void thrusterPurchased(int playerNumber)
    {
        if (playerNumber == 1)
            p1Thruster.text = "Use 'd' to activate Planetary Thruster";
    }

    public void updateThruster(float amount)
    {
        p1Thruster.text = "Use 'd' to Thrust.  FUEL :: " + amount.ToString("0.00");
    }

    public void rocketReloading(float reloadTime)
    {
        rocketReload = reloadTime;
        isLoaded = false;
        reloadTimer = reloadTime;
    }

    public void resetPurchases()
    {
        isLoaded = true;
        p1Rotator.text = rotatorDefaultStr;
        p1Thruster.text = thrusterDefaultStr;
        p1LaunchPad.text = missileDefaultStr;
    }
}
