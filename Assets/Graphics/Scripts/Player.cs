﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public int playerNumber = 1;
    public float speed = 2f;

    Rigidbody2D rigidBody;
    AI ai;
    bool isAI = false;

    GameManager gm;
    UIManager uim;


    float input = 0f;

    Vector2 startPos;

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        ai = GetComponent<AI>();
        if (ai != null)
            isAI = true;

        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        uim = GameObject.Find("UI").GetComponent<UIManager>();
        

        startPos = transform.position;
	}

    public void reset()
    {
        transform.position = startPos;
    }
	
	// Update is called once per frame
	void Update () {


        input = 0f;
        if (!isAI) // human player
        {
            // TODO: move input somewhere else
            input = Input.GetAxis("Vertical");
        }
        else // computer player
        {
            input = ai.getMove();
        }
        
	}

    private void FixedUpdate()
    {
        Vector2 vel = rigidBody.velocity;
        vel.y = input * speed; //Mathf.Clamp(input, -speed, speed);
        rigidBody.velocity = vel;
        rigidBody.position = new Vector2(startPos.x, rigidBody.position.y);

        /*
        rigidBody.AddForce(new Vector2(0, input * accelRate / Time.fixedDeltaTime));

        float pushBack = 0f;
        if (rigidBody.velocity.y > speed)
            pushBack += speed - rigidBody.velocity.y;
        else if (rigidBody.velocity.y < -speed)
            pushBack += speed - rigidBody.velocity.y;
        Debug.Log(pushBack);
        rigidBody.AddForce(new Vector2(0, pushBack * accelRate));
        */
    }
}
