﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {

    public Rigidbody2D world1, world2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public float getMove()
    {
        if (world1.velocity.x < 0 && world2.velocity.x < 0)
        {
            if (world1.position.x < world2.position.x)
                return getInput(world1.position);
            return getInput(world2.position);
        }

        else if (world1.velocity.x < 0)
        {
            return getInput(world1.position);
        }

        else if (world2.velocity.x < 0)
        {
            return getInput(world2.position);
        }

        return getInput(new Vector2(0, 5)); // default position, in the middle
    }

    private float getInput(Vector2 target)
    {
        if (transform.position.y < target.y)
            return 1f;

        return -1f;
    }
}
