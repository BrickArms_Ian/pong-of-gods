﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleImpact : MonoBehaviour {

    ParticleSystem ps;

	// Use this for initialization
	void Start () {
        ps = transform.GetChild(0).GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void impact(float angle)
    {
        transform.eulerAngles = new Vector3(0, 0, angle);
        ps.Play();
    }
}
