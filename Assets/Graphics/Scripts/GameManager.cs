﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public Player player1, player2;
    public World world1, world2;

    UIManager uim;

    public AudioClip[] audioClips;
    AudioSource audioSource;

    int[] scores = new int[2]; // pretty much   p1Score, p2Score   but works easier with playerNumber


	// Use this for initialization
	void Start () {
        uim = GameObject.Find("UI").GetComponent<UIManager>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
            SceneManager.LoadScene("titleScreen");
	}

    private void nextRound()
    {
        uim.resetPurchases();
        uim.updateInfluence(1, 0);
        uim.updateInfluence(2, 0);
        player1.reset();
        player2.reset();
        world1.gameObject.SetActive(true);
        world1.reset();
        world2.gameObject.SetActive(true);
        world2.reset();
    }

    public void worldKilled(int playerNumber)
    {
        world1.gameObject.SetActive(false);
        world2.gameObject.SetActive(false);

        Debug.Log(playerNumber);
        if (playerNumber == 1) // if player 1 died, give point to player 2
        {
            scores[1]++;
            uim.updateScore(2, scores[1]);
            audioSource.PlayOneShot(audioClips[1]);
        }
        else // if player 2 died, give point to player 1
        {
            scores[0]++;
            uim.updateScore(1, scores[0]);
            audioSource.PlayOneShot(audioClips[0]);
        }

        Debug.Log("p1: " + scores[0] + "p2: " + scores[1]);

        Invoke("nextRound", 0.5f);
    }

    public void goalHit(int side) // 0 = left, 1 = right
    {
        world1.gameObject.SetActive(false);
        world2.gameObject.SetActive(false);
        scores[side]++;
        uim.updateScore(side + 1, scores[side]);
        Invoke("nextRound", 1f);
        if (side == 0)
            audioSource.PlayOneShot(audioClips[0]);
        else
            audioSource.PlayOneShot(audioClips[1]);
    }
}
