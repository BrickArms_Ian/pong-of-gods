﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {


    public int playerNumber = 1;
    public int hitPoints = 2;
    public float sqrSpeed = 3f;
    public float maxThrusterFuel = 0.4f;
    public float rocketReload = 2f;

    public AudioClip[] acHit;
    public AudioClip upgradeSound;
    public AudioClip reloadSound; // take this out if it sucks

    AudioSource audioSource;

    float influence = 0;

    float thrusterFuel;
    int currentHitPoints;

    public Sprite[] ballSprites;

    float reloadTimer = 0f;

    public GameObject thrusterPrefab;
    public GameObject rocketPrefab;
    public GameObject launchPadPrefab;

    GameObject thruster;
    ParticleSystem thrusterParticleSys;

    ParticleImpact particleImpact;

    GameObject launchPad;
    Transform rocketSpawn;
    GameObject rocket;

    SpriteRenderer spriteRender;


    Rigidbody2D rigidBody;

    bool hasThruster = false;
    bool hasLaunchPad = false;
    bool canRotate = false;
    bool rocketLoaded = false;

    bool isThrusting = false;


    public bool isRocketLoaded() { return rocketLoaded; }
    public bool isThrusterActive() { return isThrusting; } // not used

    GameManager gm;
    UIManager uim;

    bool isAI = false;
    AIWorld ai;

    Vector2 startPos;

    void Start () {
        rigidBody = GetComponent<Rigidbody2D>();

        spriteRender = transform.GetChild(0).GetComponent<SpriteRenderer>();
        spriteRender.sprite = ballSprites[hitPoints - 1];

        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        uim = GameObject.Find("UI").GetComponent<UIManager>();

        particleImpact = transform.GetChild(1).GetComponent<ParticleImpact>();

        audioSource = GetComponent<AudioSource>();

        startPos = transform.position;
        currentHitPoints = hitPoints;

        ai = GetComponent<AIWorld>();
        if (ai != null)
        {
            isAI = true;
            influence = 1000f;
            ai.reset();
        }


        reset();

    }

    public void reset()
    {
        Destroy(launchPad);
        Destroy(rocket);
        Destroy(thruster);

        hasThruster = false;
        hasLaunchPad = false;
        canRotate = false;
        rocketLoaded = false;

        isThrusting = false;

        spriteRender.sprite = ballSprites[hitPoints - 1];
        thrusterFuel = maxThrusterFuel;
        currentHitPoints = hitPoints;

        transform.position = startPos;
        rigidBody.velocity = Vector2.zero;

        influence = 0f;
        if (isAI)
        {
            influence = 1000f;
            ai.reset();
        }


        Vector2 initialForce = new Vector2();
        initialForce.y = Random.Range(-200f, 200f);
        if (playerNumber == 1)
            initialForce.x = Random.Range(100f, 300f);
        else
            initialForce.x = Random.Range(-300f, -100f);

        rigidBody.AddForce(initialForce);
    }

    public void buyThruster()
    {
        if (influence > 50f)
        {
            thruster = (GameObject)Instantiate(thrusterPrefab, transform.position, thrusterPrefab.transform.rotation * transform.rotation);
            hasThruster = true;
            thruster.transform.SetParent(this.transform);
            thrusterParticleSys = thruster.transform.GetChild(0).GetComponentInChildren<ParticleSystem>();
            uim.thrusterPurchased(playerNumber);

            audioSource.PlayOneShot(upgradeSound);

            influence -= 50f;
        }
    }

    public void buyLaunchPad()
    {
        if (influence > 300f)
        {
            launchPad = (GameObject)Instantiate(launchPadPrefab, transform.position, launchPadPrefab.transform.rotation * transform.rotation);
            hasLaunchPad = true;
            launchPad.transform.SetParent(this.transform);
            rocketSpawn = launchPad.transform.GetChild(0).transform;
            if (!isAI)
            {
                uim.launchPadPurchased(playerNumber);
                uim.rocketReloading(rocketReload);
            }

            audioSource.PlayOneShot(upgradeSound);

            influence -= 300f;
        }
    }

    public void buyRotation()
    {
        if (influence > 100f)
        {
            canRotate = true;
            uim.rotatorPurchased(playerNumber);

            audioSource.PlayOneShot(upgradeSound);

            influence -= 100f;
        }
    }

    public void launchRocket()
    {
        rocket.GetComponent<Missile>().launch();
        rocketLoaded = false;
        if (!isAI) // TODO: remove later
            uim.rocketReloading(rocketReload);
    }

    public void activateThrust()
    {
        if (thrusterFuel > 0)
        {
            thrusterParticleSys.Play();
            isThrusting = true;
        }
    }

    public void deactivateThrust()
    {
        thrusterParticleSys.Stop();
        isThrusting = false;
    }

    public void rotateWorld(float amount)
    {
        if (canRotate)
        {
            rigidBody.AddTorque(amount * 40f);
        }
    }

	
	// Update is called once per frame
	void Update () {

        influence += 30f * Time.deltaTime;

        if (thrusterFuel < maxThrusterFuel)
            thrusterFuel += 0.1f * Time.deltaTime;

        if (!isAI)
        {
            uim.updateInfluence(playerNumber, influence);
            if (hasThruster)
                uim.updateThruster(thrusterFuel);
        }


        ////////////////// THESE INPUTS ARE TESTS \\\\\\\\\\\\\\\\

        // buy thruster
        if (Input.GetAxis("p" + playerNumber + "_Thruster") > 0 && !hasThruster)
        {
            buyThruster();
            /*
            thruster = (GameObject)Instantiate(thrusterPrefab, transform.position, thrusterPrefab.transform.rotation * transform.rotation);
            hasThruster = true;
            thruster.transform.SetParent(this.transform);
            thrusterParticleSys = thruster.GetComponentInChildren<ParticleSystem>();
            */
        }


        // buy launchpad
        if (Input.GetAxis("p" + playerNumber + "_LaunchMissile") > 0 && !hasLaunchPad)
        {
            buyLaunchPad();
            /*
            launchPad = (GameObject)Instantiate(launchPadPrefab, transform.position, launchPadPrefab.transform.rotation * transform.rotation);
            hasLaunchPad = true;
            launchPad.transform.SetParent(this.transform);
            rocketSpawn = launchPad.transform.GetChild(0).transform;
            */
        }


        // buy rotation
        if (Input.GetAxis("p" + playerNumber + "_Rotation") != 0 && !canRotate)
            buyRotation(); //canRotate = true;


        // launch rocket
        if (Input.GetAxis("p" + playerNumber + "_LaunchMissile") > 0 && rocketLoaded && hasLaunchPad)
        {
            launchRocket();
            /*
            rocket.GetComponent<Missile>().launch();
            rocketLoaded = false;
            */
        }

        // rotate world
        rotateWorld(Input.GetAxis("p" + playerNumber + "_Rotation"));

        /*
        if (Input.GetKey("q") & canRotate)
            rigidBody.AddTorque(40f);
        if (Input.GetKey("e") & canRotate)
            rigidBody.AddTorque(-40f);
            */
        
        
        // activate thrust
        if (Input.GetAxis("p" + playerNumber + "_Thruster") > 0 & hasThruster)
        {
            activateThrust();
            /*
            if (thrusterFuel > 0)
                thrusterParticleSys.Play();
            */
        }
        

        /*
        if (Input.GetKey("d") & hasThruster) // TODO: maybe just use a flag?
        {
            if (thrusterFuel > 0)
            {
                float angle = rigidBody.rotation * Mathf.Deg2Rad;
                rigidBody.AddForce(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * 3f);
                thrusterFuel -= Time.deltaTime;
            }
        }
        */

        if (Input.GetAxis("p" + playerNumber + "_Thruster") == 0 && hasThruster)
        {
            deactivateThrust();
            /*
            thrusterParticleSys.Stop();
            */
        }

        //////////////////////////////////////////////////////////


        if (isThrusting)
        {
            if (thrusterFuel > 0)
            {
                float angle = rigidBody.rotation * Mathf.Deg2Rad;
                rigidBody.AddForce(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * 4f);
                thrusterFuel -= 2f * Time.deltaTime;
            }
            else
                deactivateThrust();
        }


        // rocket reload
        if (!rocketLoaded && hasLaunchPad)
            reloadTimer += Time.deltaTime;

        if (reloadTimer > rocketReload)
        {
            // reload rocket
            rocket = (GameObject)Instantiate(rocketPrefab, rocketSpawn.position, rocketSpawn.rotation);
            rocket.transform.SetParent(this.transform);
            Physics2D.IgnoreCollision(rocket.GetComponent<Collider2D>(), transform.GetComponent<Collider2D>()); // disables collision between rocket and own planet

            audioSource.PlayOneShot(reloadSound);

            rocketLoaded = true;
            reloadTimer = 0f;
        }


        // velocity limiter
        Vector2 currentVel = rigidBody.velocity;
        float currentSpeed = currentVel.sqrMagnitude;
        if (currentSpeed > sqrSpeed)
        {
            rigidBody.velocity = currentVel * (sqrSpeed / currentSpeed);
        }
    }

    /*
    private void FixedUpdate()
    {
        rigidBody.velocity.Normalize();
        rigidBody.velocity = rigidBody.velocity.normalized * speed;
    }
    */

    private void OnCollisionEnter2D(Collision2D coll)
    {
        audioSource.PlayOneShot(acHit[Random.Range(0, acHit.Length)]);

        if (coll.gameObject.tag == "Paddle")
        {
            //float reboundY = transform.position.y - coll.gameObject.transform.position.y;
            //rigidBody.velocity = new Vector2(rigidBody.velocity.x, rigidBody.velocity.y * reboundY * 1.5f);
            //rigidBody.velocity.Normalize();


            // TODO: FIX world rebound off paddle

            Vector2 vel = rigidBody.velocity;
            float diff = transform.position.y - coll.gameObject.transform.position.y;

            if (diff > 0)
                vel.y = Mathf.Abs(vel.y);
            else
                vel.y = -Mathf.Abs(vel.y);
            rigidBody.velocity = vel;

            //rigidBody.velocity = rigidBody.velocity.normalized * speed;

            if (coll.gameObject.transform.position.x > 0)
                particleImpact.impact(90);
            else
                particleImpact.impact(-90);
        }

        else if (coll.gameObject.tag == "Missile")
        {
            Debug.Log("Hit missile");
            currentHitPoints--;

            if (currentHitPoints <= 0)
            {
                Debug.Log("Player " + playerNumber + " has LOST!");
                gm.worldKilled(playerNumber);
            }
            else
                spriteRender.sprite = ballSprites[currentHitPoints - 1];
        }

        else if (coll.gameObject.name == "LeftGoal")
        {
            gm.goalHit(0);
        }

        else if (coll.gameObject.name == "RightGoal")
        {
            gm.goalHit(1);
        }

        else if (coll.gameObject.transform.position.y > 0)
            particleImpact.impact(180);
        else
            particleImpact.impact(0);
    }
}
