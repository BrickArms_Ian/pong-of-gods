﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public float speed = 10f;
    public float deathLength = 2f;

    ParticleSystem particleSys;
    ParticleSystem explosion;
    Rigidbody2D rigidBody;

    bool hasLaunched = false;
    bool isExploding = false;

    public AudioClip[] explosionSounds;
    public AudioClip flyingSound;

    AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        particleSys = transform.GetChild(0).GetComponent<ParticleSystem>();
        explosion = transform.GetChild(1).GetComponent<ParticleSystem>();
        rigidBody = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();

        rigidBody.simulated = false;


    }

    // Update is called once per frame
    void Update()
    {
        if (isExploding)
        {
            deathLength -= Time.deltaTime;
            return;
        }

        if (hasLaunched)
        {
            float angle = (rigidBody.rotation + 90) * Mathf.Deg2Rad;
            rigidBody.AddForce(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * speed);
        }
    }

    public void launch()
    {
        transform.parent = null;
        rigidBody.simulated = true;
        hasLaunched = true;
        rigidBody.velocity = Vector2.zero; // removes world velocity, maybe keep it?
        particleSys.Play();
        audioSource.PlayOneShot(flyingSound, 0.75f);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        isExploding = true;
        rigidBody.simulated = false;
        particleSys.Stop();
        explosion.Play();
        transform.GetChild(2).gameObject.SetActive(false);
        audioSource.Stop();
        audioSource.PlayOneShot(explosionSounds[Random.Range(0, explosionSounds.Length)]);
        Destroy(this.gameObject, deathLength);
    }
}

